from django import forms
# from .models import Empleado
from .models import Cliente
from captcha.fields import ReCaptchaField


# class EmpleadoForm(forms.ModelForm):
# 	class Meta:
# 		model = Empleado
# 		fields = ('apellidos','nombres','cedula_identidad','telefono','direccion','email','estado')

class RegModelForm(forms.ModelForm):
	class Meta:
		captcha = ReCaptchaField()
		model = Cliente

		fields = ["nombre","apellidos","cedula_identidad","email","telefono","direccion"]

		def clean_email(self):
			email = self.cleaned_data.get("email")
			email_base, proveeder = email.split("@")
			dominio, extension = proveeder.split(".")
			if not extension == "com":
				raise forms.ValidationError("Por favor utiliza un email con la extension .com")
			return email
    

		def clean_nombre(self):
			nombre = self.cleaned_data.get("nombre")
			return nombre

class ContactForm(forms.Form):
	nombre = forms.CharField(required=False)
	email = forms.EmailField()
	mensaje = forms.CharField(widget=forms.Textarea)
