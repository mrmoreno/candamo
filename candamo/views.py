from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render

from .forms import RegModelForm, ContactForm
from .models import Cliente


def mostrar_inicio(request):
	return render(request, 'app/index.html', {})
	
def inicio(request):
	titulo = "Bienvenidos."
	if request.user.is_authenticated():
		titulo = "Bienvenid@ %s" %(request.user)
	form = RegModelForm(request.POST or None)

	context = {
				"titulo": titulo,
				"el_form": form,
			}

	if form.is_valid():
		instance = form.save(commit=False)
		nombre = form.cleaned_data.get("nombre")
		email = form.cleaned_data.get("email")
		if not instance.nombre:
			instance.nombre = "PERSONA"
		instance.save()

		context = {
			"titulo": "Gracias %s!" %(nombre)
		}

		if not nombre:
			context = {
				"titulo": "Gracias %s!" %(email)
			}

	if request.user.is_authenticated() and request.user.is_staff:
		queryset = Cliente.objects.all().order_by("-timestamp") 
		context = {
			"queryset": queryset,
		}
	return render(request, "inicio.html", context)

def contactr(request):
	titulo = "Contacto"
	form = ContactForm(request.POST or None)
	if form.is_valid():
			
		form_email = form.cleaned_data.get("email")
		form_mensaje = form.cleaned_data.get("mensaje")
		form_nombre = form.cleaned_data.get("nombre")
		asunto = 'Form de Contacto'
		email_from = settings.EMAIL_HOST_USER
		email_to = [email_from, "otroemail@gmail.com"]
		email_mensaje = "%s: %s enviado por %s" %(form_nombre, form_mensaje, form_email)
		send_mail(asunto, 
			email_mensaje,
			email_from,
			email_to,
			fail_silently=False
			)

			# print email, mensaje, nombre
	context = {
		"form": form,
		"titulo": titulo,
	}
	return render(request, "forms.html", context)

def mostrar_about(request):
	return render(request, 'app/about.html',{})

def mostrar_services(request):
	return render(request, 'app/services.html',{})

def mostrar_roomdetails(request):
	return render(request, 'app/room-details.html',{})

def contact(request):
		return render(request, 'app/contact.html',{})



def mostrar_login(request):
		return render(request, 'app/login.html',{})

def clienteregistrado(request):
	form = RegModelForm(request.POST or None)
	if form.is_valid():
		form_data = form.cleaned_data
		abc = form_data.get("email")
		abc2 = form_data.get("nombre")
		abc3 = form_data.get('cedula_identidad')
		abc4 = form_data.get("apellidos")
		abc5 = form_data.get("direccion")
		abc6 = form_data.get("telefono")
		obj = Cliente.objects.create(email=abc, nombre=abc2, cedula_identidad=abc3, apellidos=abc4, direccion=abc5, telefono=abc6)
		

	context = {
			"form": form,

	}
	return render(request, "app/clienteregistrado.html", context)
def contact(request):
 	form = ContactForm(request.POST or None)
 	context = {
 		"form" :form,
	
 	} 
 	return render(request, "app/contact.html", context)

