
from django.contrib import admin

from django.contrib.auth.models import User

from django.contrib.auth.admin import UserAdmin 
from .models import detalle_factura
from .models import factura

# Register your models here.
# from .models import RegModelForm
from .models import Empleado
from .models import Usuario
from .models import Cliente
from .models import Producto




class AdminCliente(admin.ModelAdmin):
	list_display =["nombre","apellidos","email","timestamp"]
	# form = RegModelForm
	list_filter = ["timestamp"]
	list_editable = ["apellidos"]
	search_fields = ["email", "nombre"]
	class Meta:
		model = Cliente

class AdminEmpleado(admin.ModelAdmin):
	list_display =["nombres","apellidos","email","direccion"]
	list_filter = ["timestamp"]
	list_editable = ["apellidos"]
	search_fields = ["email", "nombres"]
	class Meta:
		model = Empleado

class AdminUsuario(admin.ModelAdmin):
	list_display =["usuario"]
	class Meta:
		model = Usuario

class AdminProducto(admin.ModelAdmin):
	list_display =["nombre","marca"]
	list_filter = ["timestamp"]
	list_editable = ["marca"]
	search_fields = ["marca", "nombre"]
	class Meta:
		model = Producto



admin.site.register(Empleado,AdminEmpleado)
admin.site.register(Usuario)
admin.site.register(Cliente,AdminCliente)
admin.site.register(Producto,AdminProducto)