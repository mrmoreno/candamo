# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from . import views
from django.contrib.auth.views import login, password_reset, password_reset_done, password_reset_confirm, password_reset_complete
# admin.autodiscober()

urlpatterns = [
    url(r'^$', views.mostrar_inicio),
    url(r'^about/$', views.mostrar_about, name= 'about'),
    url(r'^services/$', views.mostrar_services, name= 'services'),
    url(r'^room-det/$', views.mostrar_roomdetails, name= 'room-det'),
    url(r'^login/$', views.mostrar_login, name= 'login'),
    url(r'^contact/$', views.contact, name= 'contact'),
    url(r'^clientereg/$', views.clienteregistrado, name= 'clientereg')
    # url(r'^contactr/$', views.contactr, name= 'contactr'),
    # url(r'^reset/password_reset/$', 'django.contrib.auth.views.password_reset', name='reset_password_reset1'),
    # url(r'^reset/password_reset/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    # url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm', name='password_reset_confirm'),
    # url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),
    
  	# url(r'^reset/done', password_reset_complete, {'template_name':'recuperar/password_reset_complete.html'},name="password_reset_complete"),﻿
    # url(r'^reset/password_reset/$',password_reset_done, name= 'reset_password_reset1'),
    # url(r'^reset/password_reset/done/$','django.contrib.auth.views.password_reset_done', name= 'password_reset_done'),
    # url(r'^reset/?P<uidb64>[0-94-Za-z_\-]+)/(?P<token>.+)/$','django.contrib.auth.views.password_reset_confirm', name= 'password_reset_confirm'),
    # url(r'^reset/done/$',password_reset_complete, {'template_name':'recuperar/password_reset_complete.html'},name="password_reset_complete"),﻿
    # url(r'^reset/done/$',password_reset_complete, name= 'password_reset_complete'),
    
    
    
    # url(r'^reset/password_reset',password_reset,{'template_name':'recuperar/password_reset_form.html','email_template_name':'recuperar/password_reset_email.html'},name="password_reset"),
    # url(r'^password_reset_done',password_reset_done,{'template_name':'recuperar/password_reset_done.html'},name="password_reset_done"),
    # url(r'^reset/(?P<uidb64>[0-94-Za-z_\-]+)/(?P<token>.+)/$',password_reset_confirm,{'template_name':'recuperar/password_reset_confirm.html'},name="password_reset_confirm"),
 ]