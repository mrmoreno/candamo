from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models

class Empleado(models.Model):
	nombres = models.CharField(max_length=200)
	apellidos = models.CharField(max_length=200)
	cedula_identidad= models.CharField(max_length=13)
	direccion  = models.CharField(max_length=500)
	telefono = models.CharField(max_length=14)
	email = models.EmailField()
	estado = models.BooleanField()
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	
	

	def __str__(self):
		return self.nombres

class Usuario(models.Model):
	usuario = models.CharField(max_length=200)
	password = models.CharField(max_length=200)
	empleado = models.ForeignKey('Empleado')
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
		
	def __str__(self):
		return self.usuario	

class Cliente(models.Model):
	nombre = models.CharField(max_length=200, blank=True, null=True)
	apellidos = models.CharField(max_length=200)
	cedula_identidad= models.CharField(max_length=13)
	direccion  = models.CharField(max_length=500)
	telefono = models.CharField(max_length=14)
	usuario = models.CharField(max_length=200)
	password = models.CharField(max_length=200)
	email = models.EmailField()
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __str__(self):
		return self.nombre

class Producto(models.Model):
	id_Producto = models.AutoField(primary_key=True)
	nombre = models.CharField(max_length=200)
	categoria = models.CharField(max_length=200)
	marca= models.CharField(max_length=20)
	tipo  = models.CharField(max_length=20)
	stock = models.IntegerField()
	precio_compra = models.DecimalField(max_digits=3,decimal_places=2)
	precio_venta = models.DecimalField(max_digits=3,decimal_places=2) 
	descripcion = models.CharField(max_length=200)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __str__(self):
		return self.nombre

class factura(models.Model):
	nombre = models.CharField(max_length=40, blank=True)
	nit_cliente = models.CharField(max_length=10, blank=True)
	fecha = models.DateField(null=True)
	total = models.IntegerField(null=True, blank=True)
	def  __str__(self):
		return self.total

class detalle_factura(models.Model):
	factura = models.ForeignKey(factura, db_column='factura_id')
	cliente = models.IntegerField(null=True, blank=True)
	producto = models.ForeignKey(Producto, db_column='Producto_id')
	cantidad = models.IntegerField()
	subtotal = models.IntegerField()
	def  __str__(self):
		return self.subtotal
