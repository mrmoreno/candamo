                                             
var gulp =require('gulp'),
	less =require('gulp-less'),
	rename =require('gulp-rename');
	// gulprun =require('gulp-run'),
	// pug = require('gulp-pug'),

var route={
	less:{
		entry: './src/assets/stylesheets/components.less',
		dest:  './static/css/',
		lib: './src/assets/stylesheets/**/*.less',
	},
	// pug:{
	// 	entry: './app/lib/assets/views/*.pug',
	// 	dest: './templates/',
	// 	lib: 'app/lib/assets/viess/**/*.pug'
	// },
};


gulp.task('bootstrap', ()=>{
	return gulp.src('./node_modules/bootstrap/less/bootstrap.less')
		.pipe(less())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(route.less.dest))
});

gulp.task('css', ()=>{
	return gulp.src(route.less.entry)
		.pipe(less())
		.pipe(rename('styless.css'))
		.pipe(gulp.dest(route.less.dest))
});

// gulp.task('views', ()=>{
// 	return gulp.src(route.pug.entry)
// 		.pipe(pug({
// 			pretty: true
// 		}))
// 		.pipe(gulp.dest('./templates/'))
// });

gulp.task('watch', ()=>{
	gulp.watch(route.less.lib, ['css'])
	gulp.watch('node_modules/bootstrap/less/bootstrap.less', ['bootstrap'])
});

// gulp.task('server', ()=>{
// 	electron.start();
// 	gulp.watch('./app/lib/assets/stylesheets/**/*.less',['css']);
// 	gulp.watch('./app/lib/assets/stylesheets/**/*.less', electron.restart);
// 	gulp.watch(['./templates/index.html'], electron.reload);
// });

gulp.task('default', ['watch']);