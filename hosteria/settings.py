import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'o0g87h+97c+q1^a!+yghay%$$q(0o8q8mzukb1klj77o7tbufa'
DEBUG = True

ALLOWED_HOSTS = []


EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'maycol9ronald99@gmail.com'
EMAIL_HOST_PASSWORD = '.r4e3w2q1'
EMAIL_PORT = 587
EMAIL_USE_TLS = True



# EMAIL_USE_TLS =True
# EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_PORT = 587
# EMAIL_HOST_USER ='jqwery59@gmail.com'
# EMAIL_HOST_PASSWORD ='briled99'
# DEFAULT_FROM_EMAIL = 'maycol9ronald99@gmail.com'
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))


INSTALLED_APPS = [
    'jet.dashboard',
    'jet',
    'captcha',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'livereload',
    'crispy_forms',
    'registration',
    'django_gulp',
    'django.contrib.staticfiles',
    'candamo',
]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'livereload.middleware.LiveReloadScript',
]


ROOT_URLCONF = 'hosteria.urls'
CRISPY_TEMPLATE_PACK = 'bootstrap3'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, "templates"),],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'hosteria.wsgi.application'



# LOGIN_REDIRECT_URL ='/users/account/'

# LOGIN_URL = '/users/login/'

# LOGOUT_URL = '/users/logout/'

# STATIC_ROOT = '/home/ronald/hosteria/static/'#for photo upload

STATIC_URL = '/static/'



# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'Candamo',
        'USER': 'root',
        'PASSWORD': 'ronald99',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Django JET

JET_DEFAULT_THEME = 'default'
JET_THEMES = [
    {
        'theme': 'default',
        'color': '#47bac1',
        'title': 'Default'
    },
    {
        'theme': 'green',
        'color': '#44b78b',
        'title': 'Green'
    },
    {
        'theme': 'light-green',
        'color': '#2faa60',
        'title': 'Light Green'
    },
    {
        'theme': 'light-violet',
        'color': '#a464c4',
        'title': 'Light Violet'
    },
    {
        'theme': 'light-blue',
        'color': '#5EADDE',
        'title': 'Light Blue'
    },
    {
        'theme': 'light-gray',
        'color': '#222',
        'title': 'Light Gray'
    }
]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


NOCAPTCHA = True
RECAPTCHA_PUBLIC_KEY = '6LdzSykUAAAAAD7UdenmFrz680YHiYRiw5PvGsIa'
RECAPTCHA_PRIVATE_KEY = '6LdzSykUAAAAAFugwWFRznu3IeOhKhlrso6CjZ3h'


STATICFILES_DIRS=(
    os.path.join(BASE_DIR, 'static'),
)
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/
MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media_cdn")
# MEDIA_ROOT = os.path.join(PROJECT_ROOT,"media")

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static_cdn")
# STATICFILES_DIRS = (
#     os.path.join(PROJECT_ROOT, "static"),
#     )
ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_AUTO_LOGIN = True
SITE_ID = 1
LOGIN_REDIRECT_URL = '/'
