
from django.conf import settings

from django.conf.urls import url, include
from django.conf import settings

from django.conf.urls.static import static
from django.contrib import admin
from candamo import views 






urlpatterns = [
	url(r'^jet/', include('jet.urls', 'jet')),
	url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^admin/', admin.site.urls),
    url(r'', include('candamo.urls')),
    url(r'^accounts/', include('registration.backends.default.urls')),

    

]

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.STATIC_URL, document_root=settings.MEDIA_ROOT)